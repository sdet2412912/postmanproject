# PostmanProject

## Postman 

1) This project we have developed using postman tool.

2) We have used postman and pm library that is made up from java script and java script based mocha and chai framework.

3) First we have done the postman installation setup.

## Collection

4) Under postman we have created workspace, and under the workspace we create the collections.


5) Then we created under collection and folders, multiple API�s request.
   We used rest supported methods like Get (to retrieve data form resource), Post (to create resource), Put(to update and replace), Patch(to update and modify) and Delete(to delete existing record).

6) Using restful methods we have sent the request under postman (with parameter likes API method, endpoint, authentication/authorization pre-request script, parameter, request body and test script) and getting response in the form of status code and response body.

7) Then we validate the response using test() function present in pm library.

8) We write the validation automation script in �test script�.
   1- Validate status code using snippet 
   2- Validate response body
      -fetch response body (pm.response.json())and store in local variable
      -fetch each parameter from response Body and store in local variable
      -fetch request Body (pm.request.body.raw())and store in local    variable 
      -fetch each parameter from request Body and store in local variable.
     - Validate responsebody by matching expected (response parameter) equals to actual (request parameter).
 

## Variables

9) We have used multiple variables like global , environment(Create it so our collection should be mappable with multiple environment), collection (this create at collection level and it accessible to all API within that collection), local (we create it in prescript and test script using var/let keywords).

## Automate API

10) We have automated our API�s using the collection runner and run all APIS in a single click on run tab.

## Data driven testing 

11) We have tested our API with data driven testing using random test data that getting from faker library.
 - to access it we write a script name and job (Request body parameter) in prerequisite script using pm.variables.replaceIn(�{{$RandomName}}�); and store in local variable. 
 - then this random data value set at collection level using pm.collectionVariables.set(�key�, value); 
 - then run the collection by passing collection variable key in req_body as a value ({{coll_variable key}} for req_body parameter key.

12) As well as our external test data we have prepared using .csv and .json files then pass to collection level.


## Logs

13) We used postman console to see API�s related logs/transaction.


## Sync API and Async API
14) to get instantaneously response that is sync api that we have used in this project.
Async api- don�t get instantaneously response that is async api but in response we get the two things
 - status code 202(Accepted)
 - Tracking ID(to track response using tool like splank/AWS lamda).



## Postman Reports (json)

15) After test execution we Export the report from the postman that is in json format but it�s difficult to understand to everyone for that we used Newman tool.


##Newman Report (html/htmlextra)

16) Newman it is command line interface that we use for generate easy and understandable format report.

17) That report generated in html/htmlextra format.

18) First we need to export collection, environment from postman and store in file.

18) Then run this files on Newman interface using command �newman run {location of collection with name and extension} �e {location of environment with name and extension} �reporters htmlextra �reporter-htmlextra-export {location of report where u want to store} �.

## Request Chaining and to Retry API

19) When we want to perform end to end testing for that we used request chaining concept.
Using setNextRquest (�next method name�) present in postman library.
20)our test Environment having limited memory due to that some time our request is getting fail(not pass in one go) 
  - To avoid this issue we have retry that in 5 times.
  - set the collection variable start and end 
  - Access collection variable in test script using     pm.collectionVariable.get(�key�)
  - using if else statement and �eval� function to do mathematical operations.
  - After validation need to reset collection variable using  pm.collectionVarible.set(�key�,0);











